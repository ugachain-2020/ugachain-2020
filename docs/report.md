<img src="assets/uga.png"  alt="Logo UGA"><img src="assets/polytech.png"  alt="Logo Polytech">

# Projet UGAChain
#### Lucas Reygrobellet, Baptiste Betend, Maxence Bres, Antoine Dumenil
<img src="assets/ugachain.png"  alt="Logo UGAChain">

#### Professeur encadrant : Didier Donsez

## Présentation du projet
D’après des études, 30% des CV seraient imprécis ou falsifiés (diplôme non obtenu, stage ou expérience inventé). Pour palier ce problème le projet UGAChain a vu le jour.

Le principe de cette application est de se servir de la blockchain afin de vérifier la validité des diplômes transmis par l’université. Pour cela l’université qui attribue le diplôme à un étudiant devra téléverser ce diplôme dans la blockchain. Dans un second temps l’entreprise qui voudra embaucher un étudiant pourra vérifier le diplôme à l’aide de l’application web. L’entreprise va téléverser le fichier sur l’application qui va ensuite vérifier sa présence dans la blockchain. L’application va ensuite signaler à l’entreprise si le diplôme est valide ou non.

L’application permet aussi aux membres de l’administration qui utilisent l’application d’invalider des diplômes et les considérer comme frauduleux. Il est également possible de rendre un diplôme valide à nouveau, après une précédente invalidation.

Pour pouvoir effectuer ces opérations de validation et d’invalidation, il est nécessaire de se connecter en temps qu’admin. Pour simplement vérifier la validité d’un diplôme (ce qui est le cas d’usage classique pour une entreprise), il n’est pas nécessaire de se connecter.

## Cahier des charges
Afin d’évoluer et de se rapprocher d’une version utilisable par les différentes administrations universitaires et les différentes entreprises, nous avions plusieures tâches importantes à effectuer. Se familiariser avec le projet est une tâche très importante quand on reprend un projet. Il faut en effet comprendre et réussir à utiliser l’application faite par le passé, afin de pouvoir évaluer ce qui peut être amélioré ou ce qui ne fonctionne plus et doit être refait.

Pour la suite, les premières tâches s'orientent vers la mise à jour d’Hyperledger Fabric vers la version 2.0, qui est la dernière version cette plateforme. Le front end étant pour l’instant très proche du frontend de base qui a été généré par Jhipster, il a   besoin d’être refait pour le rendre plus user-friendly, et plus proche des codes visuels de l’UGA. Le page d’authentification de keycloak a également besoin de personnalisation, afin d’être cohérente avec le reste de l’application UGA Chain.

La mise en production sur 3 machines virtuelles de l’uga est alors l’étape suivante afin d’avoir une version stable qui fonctionne. 

La dernière tâche souhaitée est la génération d’une telle application pour pouvoir l’utiliser pour d’autre type de donnée, exemple : crédit, certification ISO. En partant de l’application faite et mis à jour, faire une application squelette. 

## Technologies employées
### JHipster
Nous avons utilisé JHipster pour deux tâches différentes, la première est que nous utilisons l’application web générer en utilisant Angular et Bootstrap pour l’interface graphique, Spring pour le backend de l’application et PostGreSQL en production pour la base de données. Nous utilisons aussi JHipster afin de pouvoir générer une application utilisant la blockchain comme la nôtre avec une interface très basique.
### Hyperledger Fabric
Pour cette nouvelle itération d’UGAChain, nous avons décidé de continuer sur Hyperledger Fabric et de le mettre à jour dans sa version la plus récente (2.0.0). Hyperledger Fabric est un ledger distribué et modulaire qui permet notamment l’utilisation de Smart Contract dans une blockchain, le tout en utilisant des langages de programmation populaires tels que Java, Go et Node.js

## Architecture et réalisation technique
### Interface utilisateur
L’interface graphique qui sera utilisé par les différents utilisateurs a été généré en angular 7 donc développé par la suite dans ce même langage. Il va majoritairement servir à la dépose de fichier numérique afin de l’ajouter dans la BlockChain qui se fera soit en drag and drop soit en allant chercher le fichier. Il y aura ensuite un menu qui sera surtout utilisé pour les administrateurs du site. Et l’utilisateur aura un visuel de l’état des requêtes faites au serveur.

On peut voir sur le diagram de séquence qui suit comment fonctionne notre application côté frontend. On va pendant cette séquence faire appel à différent service certain mis à disposition par Angular. Comme on peut le voir l’utilisateur va réagir au différent component de la page. Il va commencer par téléverser un fichier comme un dépôt de fichier classique. Le composant va ensuite sollicité le service de hashage pour hasher le fichier avec une technologie SHA-256. Ensuite le service API sera sollicité pour faire la requête en fonction de ce que veut faire l'utilisateur (Vérification, Addition, Validation, Invalidation ou frauduleux). Il va également envoyé la requête et la réponse au service de gestion de requête qui pourra retransmettre au frontend les informations nécessaires pour indiquer à l’utilisateur l’état de sa requête(En cours, Succès ou Echec).
<img src="assets/diagrammeSequence.png"  alt="Diagramme de séquence">

### Noyau fonctionnel

Le noyau fonctionnel va servir à faire la liaison entre le backend et la blockchain. Il va avoir des points d’entrées API qui va récupérer la requête de l’utilisateur avec un format spécial. Pour les requêtes qui sont correcte et qui sont transmise en module de communication avec la blockchain, elles seront stockées dans une base de donnée de logs pour tracer toutes ces requêtes. Elles seront culstable via l’interface graphique en tant qu’administrateur. Ensuite à l’aide d’un package java, et ses sous paquet “request” et “blockchainException”, on va pouvoir manipuler la blockchain à partir du backend.
<img src="assets/kernel.png"  alt="Noyau fonctionnel">

### Blockchain

La première étape de la mise a jour de la blockchain fut de télécharger les nouvelles images docker de hyperledger 2.0 avec curl -sSL https://bit.ly/2ysbOFE | bash -s (https://hyperledger-fabric.readthedocs.io/en/release-2.0/install.html). Une fois les images téléchargées, il a fallu executer le script byfn.sh generate/up qui lance les script de la blockchain. L'installation préalables de Docker, GO, et curl est nécéssaire a partir de cette étape. C'est a partir de la que les multiples modifications pas à pas on commencé pour débugger la blockchain. Arrivé a la fin de la résolution de tout les problèmes la blockchain se générait en 2.0. Pour vérifier qu'elle est bien généré en 2.0 regardez au début de l'execution du up et la version est indiqué.

## Générateur de blockchain JHipster
En parallèle du développement de l’application UGA Chain, nous avons également maintenu un générateur d’application jhipster, qui permet de générer une application jhipster comportant une blockchain Hyperledger Fabric 1.4 ou 2.0. Nous avons pour cela repris le générateur du projet de l’an dernier, et nous l’avons rendu compatible avec la dernière version de Jhipster, la version 6.8.0.

Nous avons également ajouté des fonctionnalités pour le rendre plus générique, puisque dans le générateur initial, le nom des channels était fixe et s’appelait toujours “diplomas”. L’utilisateur peut maintenant au moment de la génération de son application choisir le nom qu’il souhaite en fonction des entités manipulées dans son application.

De plus l’utilisateur peut choisir au moment de la génération de son application, la version d’Hyperledger Fabric qu’il veut utiliser en choisissant entre la version 1.4 et la version 2.0.

Le fonctionnement de ce générateur est le suivant. Tout d’abord l’utilisateur génère une application jhipster classique. Une fois cette application générée, l’utilisateur va lancer notre générateur de blockchain, qui va lui demander de fournir un fichier jdl (Jhipster Domain Language) comportant la liste des entités qu’il veut créer, lui demander la version d’Hyperledger Fabric qu’il souhaite utiliser, et demander le nom du channel à créer sur la blockchain Hyperledger.

Le générateur va alors ajouter un dossier “fabric-network” à la racin du projet, comportant les fichiers nécessaires pour lancer cette blockchain, ainsi que modifier des fichiers dans le backend pour que l’application puisse interagir avec cette blockchain.
## Gestion de projet
Nous sommes partis d’un projet déjà commencé il y a deux ans et refais l’année dernière donc il a eu une étape de compréhension du projet et de sa façon de fonctionner. Les tests de l’application sur nos propres machines fait partis de ce premier sprint de découverte qui a duré environ 2 semaines.

A la suite de ce premier sprint nous avons vu avec notre enseignant référent les différentes modifications qui pouvaient être ajouté à ce projet pour lui ajouter de la valeur. On a pu ensuite diviser notre groupe de projet en 3 parties :
* Baptiste et Lucas : mis à jour d’hyperledger
* Antoine : ajout et refont du front end
* Maxence : Génération d’une application.

Afin de suivre au mieux les activités de chacun nous avons décidé de faire des sprints d’une semaine pour faire des revus régulières sur le travail qui avait été fait. La mis à jour d’hyperledger a pris plus de temps que prévu, il n’y avait aucune indication sur la progression de la mise à jour donc il était dur de planifier tant que ce n’était pas fini. Cette mis à jour nous à pris une bonne partie de la durée du projet. Ce qui a posé quelques problème car la génération de l’application était dépendant de cet avancement.

Nous avions également des contacts de l’UGA afin de pouvoir partagé et discuté des améliorations et de leurs envies. Cependant nous avions eu aucun retour de leur part ce qui est dommage au vu de l’implication qu’on a sur ce projet et ça a été un frein sur le début du projet avec l’incompréhension de certain point et du fonctionnement de l’application.

Pour ce qui est de la dernière tâche du cahier des charges c’est-à-dire la mise en production de l’application sur des VMs de l’université. Nous l’avons malheureusement pas faite par manque de temps car certaines tâches ont pris beaucoup plus de temps et de moyens que prévu. Il est toujours difficile de suivre le temps planifié pour certaines tâches car les problèmes peuvent affluer et cela mène à de la perte de temps. 
## Outils
### Outils de développement
Pour ce qui est du développement nous avons choisi d’utiliser visual code, tous les membres de l’équipe confronter à du développement utiliser le même outil, ce qui permettait une harmonie et une facilité d’utilisation.
### Outil de versionnage
Pour le versionnage, nous nous sommes servis de Git afin de pouvoir partager le code. On a aussi pu diviser en plusieurs branche pour bien diviser les tâches et en aucun cas mélanger. Gitlab nous permettait aussi de pouvoir créer des pipeline avec des jeux de test pour confirmer le bon fonctionnement de notre code en cas de modification.
### Outils de collaborations
Pour tout ce qui était gestion de projet, il y avait tout d’abord la communication qui se faisait par Slack car c’est une option simple et efficace pour un groupe de 4 personnes. Nous avons pu grâce à ce canal partager nos informations, problèmes et tout type d’erreur susceptible d’être connu par un membre du groupe.

Il y a ensuite un outil de planification, nous avons choisi trello car il nous permet de créer des tâches et une assignation des membres de l’équipe sur celle-ci. Cet outil est vraiment utile pour avoir une vision global sur l’avancement du projet et sur ce qui reste à faire.

En dernier lieu, nous avons créer un répertoire avec google drive afin d’y stocker différents document partager(schéma, rapport, diaporama) afin que tous les membres du groupe puisse modifier ces documents plus simplement.
## Métriques logicielles
Nous somme parti d’un 
Au niveau du partage des langages, on observe : 52% de java, 33,5% de Typescript, 9,3% d’html, 2,6% de javascript et 2,5% de CSS.

| Élément               |	Quantité|	Coût unitaire   | Total     |
|-----------------------|-----------|-------------------|-----------|
| Salaire               |	4       |8400	            |33600      |
| Electricité           |	120	    |0.158              |18.96      |
| Nettoyage	            |35         |1.3	            |45.5       |
| Cantine/Gouter        |	35	    |2 + 1.35	        |117.25     |
| Investissements (PC)  |	2       |41                 |82         |
|         -             |      -    | Total	            |33863.71€  |

## Conclusion
Au final, ce projet a été assez compliqué à démarrer, car nous avons fait le choix de poursuivre le travail qui avait été fait l’an dernier. Il nous a donc fallu commencer par nous familiariser avec celui-ci, ce qui s’est avéré plus difficile que prévu étant donné que nous disposions de peu de documentation sur ce projet qui était assez complexe.

Néanmoins le sujet et les technologies de ce projet étaient très intéressantes, la blockchain est en effet une technologie très actuelle et il a été pour nous très intéressant de manipuler ce concept.

De plus notre organisation d’équipe et le fait que nous ayons déjà travaillé ensemble par le passé nous a grandement aidé dans la réalisation de ce projet.
## Bibliographie
* https://blockchainfrance.net/decouvrir-la-blockchain/c-est-quoi-la-blockchain/
* https://hyperledger-fabric.readthedocs.io/en/release-2.0/whatis.html
