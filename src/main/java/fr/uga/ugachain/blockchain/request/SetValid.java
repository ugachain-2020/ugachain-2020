package fr.uga.ugachain.blockchain.request;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hyperledger.fabric.sdk.ChaincodeID;
import org.hyperledger.fabric.sdk.ProposalResponse;
import org.hyperledger.fabric.sdk.TransactionProposalRequest;
import org.hyperledger.fabric.sdk.ChaincodeResponse.Status;

import fr.uga.ugachain.blockchain.Config;
import fr.uga.ugachain.blockchain.blockchainException.DiplomaNotFound;
import fr.uga.ugachain.blockchain.blockchainException.StateAlreadySet;

public class SetValid extends A_BlockchainRequest {
	private static final byte[] EXPECTED_EVENT_DATA = "!".getBytes(UTF_8);
	private static final String EXPECTED_EVENT_NAME = "event";
	private String hash = null;
	public Collection<ProposalResponse> responses;

	public SetValid(String hash) {

		/* Enroll Admin to Org1MSP and initialize channel */
		super();
		this.hash = hash;
	}

	@Override
	public void send() throws DiplomaNotFound, StateAlreadySet, Exception {

		// TODO check if present in BC
		// TODO check if not already valid
		TransactionProposalRequest request = fabClient.getInstance().newTransactionProposalRequest();
		ChaincodeID ccid = ChaincodeID.newBuilder().setName(Config.CHAINCODE_1_NAME).build();
		request.setChaincodeID(ccid);
		request.setFcn("setValid");
		String[] arguments = { hash };
		request.setArgs(arguments);
		request.setProposalWaitTime(1000);

		Map<String, byte[]> tm2 = new HashMap<>();
		tm2.put("HyperLedgerFabric", "TransactionProposalRequest:JavaSDK".getBytes(UTF_8));
		tm2.put("method", "TransactionProposalRequest".getBytes(UTF_8));
		tm2.put("result", ":)".getBytes(UTF_8));
		tm2.put(EXPECTED_EVENT_NAME, EXPECTED_EVENT_DATA);
		request.setTransientMap(tm2);
		Collection<ProposalResponse> responses = channelClient.sendTransactionProposal(request);
		for (ProposalResponse res : responses) {
			Status status = res.getStatus();

			String payload = new String(res.getChaincodeActionResponsePayload());

			if (!res.isVerified() || res.getStatus() != ProposalResponse.Status.SUCCESS) {
                Logger.getLogger(Query.class.getName()).log(Level.INFO, "Failed query proposal from peer " + res.getPeer().getName() + " status: " + res.getStatus() + ". Messages: " + res.getMessage() + ". Was verified : " + res.isVerified());
            } else {
                //String payload = res.getProposalResponse().getResponse().getPayload().toStringUtf8();
                Logger.getLogger(Query.class.getName()).log(Level.INFO, "Valide payload " + res.getPeer().getName() + payload);
                //assertEquals(payload, args1);
            }

			// Raising exception if the diploma hash isn't in the BC
			// or if the state of the diploma is already set to valid
			if (payload.equals("NOT_FOUND")) {
				throw new DiplomaNotFound("");
			}
			else if (payload.equals("STATE_ALREADY_SET")) {
				throw new StateAlreadySet("");
			}
			
			
			transactionID = res.getTransactionID();
			Logger.getLogger(Add.class.getName()).log(Level.INFO,
					"\n\n\n#----------------------------> Invoked setValid on " + Config.CHAINCODE_1_NAME
							+ ". Status - " + status + "\n\n\n");
		}

	}
}
