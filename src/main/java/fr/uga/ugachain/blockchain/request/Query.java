package fr.uga.ugachain.blockchain.request;

import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hyperledger.fabric.sdk.ProposalResponse;

import fr.uga.ugachain.blockchain.Config;
import fr.uga.ugachain.blockchain.blockchainException.DiplomaNotFound;

/*
 * Request to get the state of a diploma by hash
 */
public class Query extends A_BlockchainRequest {

	private String hash = null;
	/* Diploma state */
	public String state = null;

	public Query(String hash) {

		/* Enroll Admin to Org1MSP and initialize channel */
		super();
		this.hash = hash;
	}

	/*
	 * Query the state of the diploma related to the hash
	 */
	public void send() throws DiplomaNotFound, Exception {

		String[] args1 = { hash };
		Logger.getLogger(Query.class.getName()).log(Level.INFO,
				"Querying for the state of the diploma related to the hash: " + args1[0]);

		Collection<ProposalResponse> responses1Query = channelClient.queryByChainCode(Config.CHAINCODE_1_NAME, "query",
				args1);
		for (ProposalResponse pres : responses1Query) {
			if (!pres.isVerified() || pres.getStatus() != ProposalResponse.Status.SUCCESS) {
                Logger.getLogger(Query.class.getName()).log(Level.INFO, "Failed query proposal from peer " + pres.getPeer().getName() + " status: " + pres.getStatus() + ". Messages: " + pres.getMessage() + ". Was verified : " + pres.isVerified());
            } else {
                String payload = pres.getProposalResponse().getResponse().getPayload().toStringUtf8();
                Logger.getLogger(Query.class.getName()).log(Level.INFO, "Query payload " + pres.getPeer().getName() + payload);
                //assertEquals(payload, args1);
            }
			String stringResponse = new String(pres.getChaincodeActionResponsePayload());
			transactionID = pres.getTransactionID();
			state = stringResponse;

			// Raising exception if the diploma hash is not found
			if (state.equals("NOT_FOUND")) {
				throw new DiplomaNotFound("");
			}

			Logger.getLogger(Query.class.getName()).log(Level.INFO,
					"\n\n\n#----------------------------> Query response for the hash: " + hash + ": " + stringResponse
							+ "\n\n\n");
		}

	}
}
