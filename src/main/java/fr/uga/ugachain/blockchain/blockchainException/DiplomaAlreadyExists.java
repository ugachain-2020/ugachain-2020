package fr.uga.ugachain.blockchain.blockchainException;

public class DiplomaAlreadyExists extends A_BlockchainException {

	public DiplomaAlreadyExists(String s) {
		super(s);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "DiplomaAlreadyPresent";
	}
}
