package fr.uga.ugachain.blockchain.blockchainException;

public class DiplomaNotFound extends A_BlockchainException {

	public DiplomaNotFound(String s) {
		super(s);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "DiplomaNotFound";
	}
}
