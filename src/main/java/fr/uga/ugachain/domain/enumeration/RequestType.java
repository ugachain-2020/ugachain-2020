package fr.uga.ugachain.domain.enumeration;

/**
 * The RequestType enumeration.
 */
public enum RequestType {
    ADDITION, INVALIDATION, REVALIDATION, FRAUD_REPORT, CHECK
}
