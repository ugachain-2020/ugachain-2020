/**
 * View Models used by Spring MVC REST controllers.
 */
package fr.uga.ugachain.web.rest.vm;
