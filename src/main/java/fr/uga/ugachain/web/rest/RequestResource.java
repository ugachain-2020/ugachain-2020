package fr.uga.ugachain.web.rest;

import fr.uga.ugachain.blockchain.blockchainException.A_BlockchainException;
import fr.uga.ugachain.blockchain.blockchainException.DiplomaNotFound;
import fr.uga.ugachain.blockchain.request.Add;
import fr.uga.ugachain.blockchain.request.SetInvalid;
import fr.uga.ugachain.blockchain.request.SetValid;
import fr.uga.ugachain.blockchain.request.Query;
import fr.uga.ugachain.blockchain.request.SetFraudulent;
import fr.uga.ugachain.domain.Request;
import fr.uga.ugachain.repository.RequestRepository;
import fr.uga.ugachain.web.rest.errors.BadRequestAlertException;
import fr.uga.ugachain.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import fr.uga.ugachain.domain.enumeration.*;
import java.time.Instant;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Request.
 */
@RestController
@RequestMapping("/api")
public class RequestResource {

	private final Logger log = LoggerFactory.getLogger(RequestResource.class);

	private static final String ENTITY_NAME = "request";

	private final RequestRepository requestRepository;

	public RequestResource(RequestRepository requestRepository) {
		this.requestRepository = requestRepository;
	}

	private enum DiplomaState {
		VALID, INVALID, FRAUDULENT
	}

	/**
	 * POST /requests : Create a new request.
	 *
	 * @param request the request to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         request, or with status 400 (Bad Request) if the request has already
	 *         an ID
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@PostMapping("/requests")
	public ResponseEntity<Request> createRequest(@RequestBody Request request) throws URISyntaxException {
		log.debug("REST request to save Request : {}", request);
		if (request.getId() != null) {
			throw new BadRequestAlertException("A new request cannot already have an ID", ENTITY_NAME, "idexists");
		}
		Request result = requestRepository.save(request);
		return ResponseEntity.created(new URI("/api/requests/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /requests : Updates an existing request.
	 *
	 * @param request the request to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         request, or with status 400 (Bad Request) if the request is not
	 *         valid, or with status 500 (Internal Server Error) if the request
	 *         couldn't be updated
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@PutMapping("/requests")
	public ResponseEntity<Request> updateRequest(@RequestBody Request request) throws URISyntaxException {
		log.debug("REST request to update Request : {}", request);
		if (request.getId() == null) {
			throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
		}
		Request result = requestRepository.save(request);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, request.getId().toString()))
				.body(result);
	}

	/**
	 * GET /requests : get all the requests.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of requests in
	 *         body
	 */
	@GetMapping("/requests")
	public List<Request> getAllRequests() {
		log.debug("REST request to get all Requests");
		return requestRepository.findAll();
	}

	/**
	 * GET /requests/:id : get the "id" request.
	 *
	 * @param id the id of the request to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the request, or
	 *         with status 404 (Not Found)
	 */
	@GetMapping("/requests/{id}")
	public ResponseEntity<Request> getRequest(@PathVariable Long id) {
		log.debug("REST request to get Request : {}", id);
		Optional<Request> request = requestRepository.findById(id);
		return ResponseUtil.wrapOrNotFound(request);
	}

	/**
	 * DELETE /requests/:id : delete the "id" request.
	 *
	 * @param id the id of the request to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/requests/{id}")
	public ResponseEntity<Void> deleteRequest(@PathVariable Long id) {
		log.debug("REST request to delete Request : {}", id);
		requestRepository.deleteById(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * POST /requests/add : add a new request to the blockchain.
	 *
	 * @param login the login of the person performing the transaction
	 * @param hash  the hash of the diploma we want to add to the BC
	 * @return the ResponseEntity with status 200 (OK) and the transaction ID, or
	 *         with status 417 (EXPECTATION_FAILED), or with status 500
	 *         (INTERNAL_SERVER_ERROR)
	 */
	@PostMapping("/requests/add")
	public ResponseEntity<String> addHash(@RequestParam String login, @RequestParam String hash) {
		if (login == null || login.length() <= 0) {
			log.debug("\n" + login + "\nERROR : login is null or its length equals to 0");
			return new ResponseEntity("INVALID LOGIN", HttpStatus.EXPECTATION_FAILED);
		}
		if (hash.length() != 64) {
			log.debug("The hash does not match SHA256 format");
			return new ResponseEntity("INVALID HASH", HttpStatus.EXPECTATION_FAILED);
		}

		// Add the diploma to the blockchain
		Add addDiplomaRequest;
		String transactionID;
		try {
			addDiplomaRequest = new Add(hash);
			addDiplomaRequest.send();
			transactionID = addDiplomaRequest.transactionID;
		} catch (A_BlockchainException e) {
			String errored = "ERROR :" + e.toString();
			log.debug(errored);
			return new ResponseEntity(errored, HttpStatus.NOT_ACCEPTABLE);
		} catch (Exception e) {
			String errored = "ERROR : something happened in the add function \n\n" + e.getStackTrace()[0].toString();
			log.debug(errored);
			return new ResponseEntity(errored, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		// Log the insert operation (whether fail or not)
		Instant timestamp = Instant.now();
		Request request = new Request();
		request.setHash(hash);
		request.setType(RequestType.ADDITION);
		request.setDate(timestamp);
		request.setRequesterLogin(login);
		requestRepository.save(request);
		String returned = "{\"transactionID\":\"" + transactionID + "\"}";
		return new ResponseEntity(returned, HttpStatus.OK);
	}

	/**
	 * POST /requests/invByHash : invalid a Diploma already in the BC
	 *
	 * @param login the login of the person performing the transaction
	 * @param hash  the hash of the diploma we want to invalidate
	 * @return the ResponseEntity with status 200 (OK) and the transaction ID, or
	 *         with status 417 (EXPECTATION_FAILED), or with status 500
	 *         (INTERNAL_SERVER_ERROR)
	 */
	@PostMapping("/requests/invalidate")
	public ResponseEntity<String> addInvByHash(@RequestParam String login, @RequestParam String hash) {
		if (login == null || login.length() <= 0) {
			log.debug("\n" + login + "\nERROR : login is null or its length equals to 0");
			return new ResponseEntity("INVALID LOGIN", HttpStatus.EXPECTATION_FAILED);
		}
		if (hash.length() != 64) {
			log.debug("The hash does not match SHA256 format");
			return new ResponseEntity("INVALID HASH", HttpStatus.EXPECTATION_FAILED);
		}

		// Log the operation
		Instant timestamp = Instant.now();
		Request request = new Request();
		request.setHash(hash);
		request.setType(RequestType.INVALIDATION);
		request.setDate(timestamp);
		request.setRequesterLogin(login);
		requestRepository.save(request);

		// Add the invalid flag to the blockchain
		SetInvalid si;
		String transactionID;
		try {
			si = new SetInvalid(hash);
			si.send();
			transactionID = si.transactionID;
		} catch (A_BlockchainException e) {
			String errored = "ERROR :" + e.toString();
			log.debug(errored);
			return new ResponseEntity(errored, HttpStatus.NOT_ACCEPTABLE);
		} catch (Exception e) {
			String errored = "ERROR : something happened in the setInvalid \n\n" + e.getStackTrace()[0].toString();;
			log.debug(errored);
			return new ResponseEntity(errored, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		String returned = "{\"transactionID\":\"" + transactionID + "\"}";
		return new ResponseEntity(returned, HttpStatus.OK);
	}

	/**
	 * POST /requests/revByHash : revalid a Diploma already in the BC
	 *
	 * @param login the login of the person performing the transaction
	 * @param hash  the hash of the diploma we want to revalidate
	 * @return the ResponseEntity with status 200 (OK) and the transaction ID, or
	 *         with status 417 (EXPECTATION_FAILED), or with status 500
	 *         (INTERNAL_SERVER_ERROR)
	 */
	@PostMapping("/requests/revalidate")
	public ResponseEntity<String> addRevByHash(@RequestParam String login, @RequestParam String hash) {
		if (login == null || login.length() <= 0) {
			log.debug("\n" + login + "\nERROR : login is null or its length equals to 0");
			return new ResponseEntity("INVALID LOGIN", HttpStatus.EXPECTATION_FAILED);
		}
		if (hash.length() != 64) {
			log.debug("The hash does not match SHA256 format");
			return new ResponseEntity("INVALID HASH", HttpStatus.EXPECTATION_FAILED);
		}

		// Add the valid flag to the BC
		SetValid sv;
		String transactionID;
		try {
			sv = new SetValid(hash);
			sv.send();
			transactionID = sv.transactionID;
		} catch (A_BlockchainException e) {
			String errored = "ERROR :" + e.toString();
			log.debug(errored);
			return new ResponseEntity(errored, HttpStatus.NOT_ACCEPTABLE);
		} catch (Exception e) {
			String errored = "ERROR : something happened in the setValid \n\n" + e.getStackTrace()[0].toString();
			log.debug(errored);
			return new ResponseEntity(errored, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		// Log the operation
		Instant timestamp = Instant.now();
		Request request = new Request();
		request.setHash(hash);
		request.setType(RequestType.REVALIDATION);
		request.setDate(timestamp);
		request.setRequesterLogin(login);
		requestRepository.save(request);

		String returned = "{\"transactionID\":\"" + transactionID + "\"}";
		return new ResponseEntity(returned, HttpStatus.OK);
	}

	/**
	 * POST /requests/fraByHash : make fraudulent a Diploma already in the BC
	 *
	 * @param login the login of the person performing the transaction
	 * @param hash  the hash of the diploma we want to make fraudulent
	 * @return the ResponseEntity with status 200 (OK) and the transaction ID, or
	 *         with status 417 (EXPECTATION_FAILED), or with status 500
	 *         (INTERNAL_SERVER_ERROR)
	 */
	@PostMapping("/requests/fraudulent")
	public ResponseEntity<String> addFraByHash(@RequestParam String login, @RequestParam String hash) {
		if (login == null || login.length() <= 0) {
			log.debug("\n" + login + "\nERROR : login is null or its length equals to 0");
			return new ResponseEntity("INVALID LOGIN", HttpStatus.EXPECTATION_FAILED);
		}
		if (hash.length() != 64) {
			log.debug("The hash does not match SHA256 format");
			return new ResponseEntity("INVALID HASH", HttpStatus.EXPECTATION_FAILED);
		}

		// Add the fraud flag to the BC
		SetFraudulent sf;
		String transactionID;
		try {
			sf = new SetFraudulent(hash);
			sf.send();
			transactionID = sf.transactionID;
		} catch (A_BlockchainException e) {
			String errored = "ERROR :" + e.toString();
			log.debug(errored);
			return new ResponseEntity(errored, HttpStatus.NOT_ACCEPTABLE);
		} catch (Exception e) {
			String errored = "ERROR : something happened in the setFraudulent \n\n" + e.getStackTrace()[0].toString();
			log.debug(errored);
			return new ResponseEntity(errored, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		// Log the operation
		Instant timestamp = Instant.now();
		Request request = new Request();
		request.setHash(hash);
		request.setType(RequestType.FRAUD_REPORT);
		request.setDate(timestamp);
		request.setRequesterLogin(login);
		requestRepository.save(request);

		String returned = "{\"transactionID\":\"" + transactionID + "\"}";
		return new ResponseEntity(returned, HttpStatus.OK);
	}

	/**
	 * GET /requests/checkByHash : Check the status of a Diploma in the BC
	 *
	 * @param login the login of the person performing the transaction
	 * @param hash  the hash of the diploma we want to check
	 * @return the ResponseEntity with status 200 (OK) and the status of the
	 *         diploma, or with status 417 (EXPECTATION_FAILED), or with status 500
	 *         (INTERNAL_SERVER_ERROR)
	 */
	@GetMapping("/requests/check")
	public ResponseEntity<String> checkByHash(@RequestParam String hash) {
		String login = "Anonyme";
		if (hash.length() != 64) {
			log.debug("The hash does not match SHA256 format");
			return new ResponseEntity("INVALID HASH", HttpStatus.EXPECTATION_FAILED);
		}

		String state = null;
		DiplomaState diplomaState = null;
		Query queryByHash;

		// log the consultation operation
		Instant timestamp = Instant.now();
		Request request = new Request();
		request.setHash(hash);
		request.setType(RequestType.CHECK);
		request.setDate(timestamp);
		request.setRequesterLogin(login);
		requestRepository.save(request);

		try {
			queryByHash = new Query(hash);
			queryByHash.send();
			state = queryByHash.state;
			diplomaState = null;
		} catch (DiplomaNotFound e) {
			String errored = "ERROR :" + e.toString();
			log.debug(errored);
			String returned = "{\"diplomaState\":\"NOT_FOUND\"}";
			return new ResponseEntity(returned, HttpStatus.OK);
		} catch (Exception e) {
			String errored = "ERROR : something happened in the query \n\n" + e.getStackTrace()[0].toString();
			log.debug(errored);
			return new ResponseEntity(errored, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (state == null) {
			log.debug("The check has failed");
			return new ResponseEntity("QUERY FAILED", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		switch (state) {
		case "NOT_FOUND":
			String returned = "{\"diplomaState\":\"NOT_FOUND\"}";
			return new ResponseEntity(returned, HttpStatus.OK);
		case "VALID":
			diplomaState = DiplomaState.VALID;
			break;
		case "INVALID":
			diplomaState = DiplomaState.INVALID;
			break;
		case "FRAUDULENT":
			diplomaState = DiplomaState.FRAUDULENT;
			break;
		default:
			break;
		}

		// return in json format
		String returned = "{\"diplomaState\":\"" + diplomaState + "\"}";
		return new ResponseEntity(returned, HttpStatus.OK);

	}
}
