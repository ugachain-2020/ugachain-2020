import { Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

@Component({
    selector: 'jhi-footer',
    templateUrl: './footer.component.html'
})
export class FooterComponent {}
