export enum DiplomaState {
    VALID = 'VALID',
    INVALID = 'INVALID',
    FRAUDULENT = 'FRAUDULENT',
    NOT_FOUND = 'NOT_FOUND'
}
