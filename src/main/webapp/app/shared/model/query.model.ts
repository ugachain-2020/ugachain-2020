import { DiplomaState } from './diplomaState.model';

export enum IQueryStatus {
    RUNNING = 'RUNNING',
    SUCCESS = 'SUCCESS',
    ERROR = 'ERROR'
}

export interface IQuery {
    status: IQueryStatus;
    errorCode?: number;
    errorMessage?: string;
}

export interface IEditionQuery extends IQuery {
    hash: string;
    resultingTransactionID?: string;
}

export interface ICheckQuery extends IQuery {
    hash: string;
    resultingDiplomaState?: DiplomaState;
}
