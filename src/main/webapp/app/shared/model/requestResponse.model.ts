import { RequestType } from './request.model';
import { DiplomaState } from './diplomaState.model';

export const enum RequestResponseResult {
    SUCCESS = 'SUCCESS',
    CLIENT_ERROR = 'CLIENT_ERROR',
    SERVER_ERROR = 'SERVER_ERROR'
}

export interface IRequestResponse {
    type: RequestType;
    result: RequestResponseResult;
    errorMessage?: string;
    errorCode?: number;
}

export interface EditionResponse {
    hash: string;
    transactionID: string;
}

export interface EditionResult extends IRequestResponse, EditionResponse {}

export interface CheckResponse {
    hash: string;
    diplomaState: DiplomaState;
}

export interface CheckResult extends IRequestResponse, CheckResponse {}
