import { Moment } from 'moment';

export const enum RequestType {
    ADDITION = 'ADDITION',
    INVALIDATION = 'INVALIDATION',
    REVALIDATION = 'REVALIDATION',
    FRAUD_REPORT = 'FRAUD_REPORT',
    CHECK = 'CHECK'
}

export interface IRequest {
    id?: number;
    type?: RequestType;
    date?: Moment;
    hash?: string;
    requesterLogin?: string;
}

export class Request implements IRequest {
    constructor(
        public id?: number,
        public type?: RequestType,
        public date?: Moment,
        public hash?: string,
        public requesterLogin?: string
    ) {}
}
