export interface IUploadedFile {
    metadata: File;
    data: ArrayBuffer;
}
