import { Injectable } from '@angular/core';
import { Subject, ReplaySubject } from 'rxjs';

import { CheckResult, EditionResult, RequestResponseResult } from '../model/requestResponse.model';
import { EditionType } from '../model/edition.model';
import { IEditionQuery, ICheckQuery, IQueryStatus } from '../model/query.model';
import { RequestType } from '../model/request.model';

@Injectable({
    providedIn: 'root'
})
export class QueriesStatusService {
    private _checkQueriesStatus = new Subject<ICheckQuery>();

    private _additionQueriesStatus = new Subject<IEditionQuery>();
    private _fraudreportQueriesStatus = new Subject<IEditionQuery>();
    private _invalidationQueriesStatus = new Subject<IEditionQuery>();
    private _revalidationQueriesStatus = new Subject<IEditionQuery>();

    private hashFilenameMap: Map<string, string> = new Map<string, string>();

    constructor() {}

    public addCheckResult(hashCheckResult: ReplaySubject<CheckResult>, checkedHash: string): void {
        hashCheckResult.subscribe((checkRequestResponse: CheckResult) => {
            if (checkRequestResponse.type !== RequestType.CHECK) {
                console.log('Error :  Malformed API request to check hash !');
                console.log('For hash ', checkedHash, ' request of type ', checkRequestResponse.type);
                this.checkQueriesStatus.next({ hash: checkedHash, status: IQueryStatus.ERROR });
            } else {
                console.log(checkRequestResponse.result, ' for hash ', checkedHash, ' request of type ', checkRequestResponse.type);
                switch (checkRequestResponse.result) {
                    case RequestResponseResult.SUCCESS:
                        this.checkQueriesStatus.next({
                            hash: checkedHash,
                            status: IQueryStatus.SUCCESS,
                            resultingDiplomaState: checkRequestResponse.diplomaState
                        });
                        break;
                    case RequestResponseResult.CLIENT_ERROR:
                        this.checkQueriesStatus.next({
                            hash: checkedHash,
                            status: IQueryStatus.ERROR,
                            errorMessage: checkRequestResponse.errorMessage,
                            errorCode: checkRequestResponse.errorCode
                        });
                        break;
                    case RequestResponseResult.SERVER_ERROR:
                        this.checkQueriesStatus.next({
                            hash: checkedHash,
                            status: IQueryStatus.ERROR,
                            errorMessage: checkRequestResponse.errorMessage,
                            errorCode: checkRequestResponse.errorCode
                        });
                        break;
                    default:
                        console.log('Unsupported RequestResponseResult');
                        this.checkQueriesStatus.next({
                            hash: checkedHash,
                            status: IQueryStatus.ERROR,
                            errorMessage: checkRequestResponse.errorMessage,
                            errorCode: checkRequestResponse.errorCode
                        });
                        break;
                }
            }
        });
    }

    public addEditionResult(editionType: EditionType, hashEditionResult: ReplaySubject<EditionResult>, addedHash: string): void {
        hashEditionResult.subscribe((editionRequestResponse: EditionResult) => {
            console.log(editionRequestResponse.result, 'for hash ', addedHash, ' request of type ', editionRequestResponse.type);

            let queriesStatusSubject: Subject<IEditionQuery>;
            switch (editionType) {
                case EditionType.ADDITION:
                    queriesStatusSubject = this.additionQueriesStatus;
                    break;
                case EditionType.FRAUD_REPORT:
                    queriesStatusSubject = this.fraudreportQueriesStatus;
                    break;
                case EditionType.INVALIDATION:
                    queriesStatusSubject = this.invalidationQueriesStatus;
                    break;
                case EditionType.REVALIDATION:
                    queriesStatusSubject = this.revalidationQueriesStatus;
                    break;
                default:
                    throw 'Unsupported EditionType';
            }

            switch (editionRequestResponse.result) {
                case RequestResponseResult.SUCCESS:
                    queriesStatusSubject.next({
                        hash: addedHash,
                        status: IQueryStatus.SUCCESS,
                        resultingTransactionID: editionRequestResponse.transactionID
                    });
                    break;
                case RequestResponseResult.CLIENT_ERROR:
                    queriesStatusSubject.next({
                        hash: addedHash,
                        status: IQueryStatus.ERROR,
                        errorMessage: editionRequestResponse.errorMessage,
                        errorCode: editionRequestResponse.errorCode
                    });
                    break;
                case RequestResponseResult.SERVER_ERROR:
                    queriesStatusSubject.next({
                        hash: addedHash,
                        status: IQueryStatus.ERROR,
                        errorMessage: editionRequestResponse.errorMessage,
                        errorCode: editionRequestResponse.errorCode
                    });
                    break;
                default:
                    console.log('Unsupported RequestResponseResult');
                    queriesStatusSubject.next({
                        hash: addedHash,
                        status: IQueryStatus.ERROR,
                        errorMessage: editionRequestResponse.errorMessage,
                        errorCode: editionRequestResponse.errorCode
                    });
                    break;
            }
        });
    }

    /* Utils */

    public getQueryFilename(query: ICheckQuery): string {
        return this.hashFilenameMap.get(query.hash);
    }

    public addFilenameHashPair(uploadedFilename: string, hash: string): void {
        this.hashFilenameMap.set(hash, uploadedFilename);
    }

    /* Getters & Setters */

    public getQueriesStatusSubject(editionType: EditionType): Subject<IEditionQuery> {
        switch (editionType) {
            case EditionType.ADDITION:
                return this.additionQueriesStatus;
            case EditionType.FRAUD_REPORT:
                return this.fraudreportQueriesStatus;
            case EditionType.INVALIDATION:
                return this.invalidationQueriesStatus;
            case EditionType.REVALIDATION:
                return this.revalidationQueriesStatus;
            default:
                throw 'Unsupported EditionType';
        }
    }

    public get checkQueriesStatus() {
        return this._checkQueriesStatus;
    }

    public get additionQueriesStatus() {
        return this._additionQueriesStatus;
    }

    public get fraudreportQueriesStatus() {
        return this._fraudreportQueriesStatus;
    }

    public get invalidationQueriesStatus() {
        return this._invalidationQueriesStatus;
    }

    public get revalidationQueriesStatus() {
        return this._revalidationQueriesStatus;
    }
}
