import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, ReplaySubject, throwError } from 'rxjs';
import { map } from 'rxjs/operators/';

import { SERVER_API_URL } from 'app/app.constants';

import { AccountService } from 'app/core';
import { HashService } from './hash.service';
import { QueriesStatusService } from './queries-status.service';

import { EditionResponse, EditionResult, CheckResponse, CheckResult, RequestResponseResult } from '../model/requestResponse.model';
import { EditionType } from '../model/edition.model';
import { IQueryStatus } from '../model/query.model';
import { IUploadedFile } from '../model/uploadedFile.model';
import { RequestType } from '../model/request.model';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    constructor(
        private accountService: AccountService,
        private hashService: HashService,
        private httpClient: HttpClient,
        private queriesStatusService: QueriesStatusService
    ) {}

    /* DIPLOMA EDITION SECTION */

    /**
     *
     * @param files Files to upload
     * @returns A map with keys being uploaded files and values being hashes
     */
    public editDiplomas(files: IUploadedFile[], editionType: EditionType): Map<string, string> {
        const nameHashMap: Map<string, string> = new Map<string, string>();

        files.forEach((file: IUploadedFile) => {
            // ONLY FOR DEBUG
            // const computedHash: string = this.hashService.notWorkinghashIUploadedFile(file);

            const computedHash: string = this.hashService.hashIUploadedFile(file);
            const addHashResult: ReplaySubject<EditionResult> = this.editDiplomaHash(computedHash, editionType);

            nameHashMap.set(computedHash, file.metadata.name);

            this.queriesStatusService.addEditionResult(editionType, addHashResult, computedHash);
        });

        return nameHashMap;
    }

    public editDiplomaHash(hash: string, editionType: EditionType): ReplaySubject<EditionResult> {
        // used ReplaySubject due to answer https://stackoverflow.com/a/33676723/6304206
        const editionRequestResponseObservable: ReplaySubject<EditionResult> = new ReplaySubject<EditionResult>(1);

        // Get current user's login
        this.accountService.fetch().subscribe(httpResponse => {
            if (httpResponse.body === null || httpResponse.body === undefined) {
                return throwError('httpResponse.body === null|undefined');
            }
            if (httpResponse.body.login === null || httpResponse.body.login === undefined) {
                return throwError('httpResponse.body.login === null|undefined');
            }

            const login = httpResponse.body.login;
            const response: Observable<EditionResult> = this.editDiplomaHashWithLogin(hash, login, editionType);

            response.subscribe(
                (data: EditionResult) => {
                    editionRequestResponseObservable.next(data);
                },
                error => {
                    const errorCode: number = error.status;
                    let errorMessage: string;
                    let result: RequestResponseResult;

                    switch (error.status) {
                        case 406:
                            result = RequestResponseResult.CLIENT_ERROR;
                            errorMessage = "Le hash n'est pas présent dans la blockchain";
                            break;
                        case 417:
                            result = RequestResponseResult.CLIENT_ERROR;
                            switch (error.error) {
                                case 'INVALID HASH':
                                    console.log('Detected "INVALID HASH" error code');
                                    errorMessage = 'Le hash fourni est invalide';
                                    break;
                                case 'INVALID LOGIN':
                                    console.log('Detected "INVALID LOGIN" error code');
                                    errorMessage = "Le format de l'identifiant fourni est invalide";
                                    break;
                                default:
                                    errorMessage = error.error;
                                    break;
                            }
                            break;
                        case 500:
                            result = RequestResponseResult.SERVER_ERROR;
                            errorMessage = 'Erreur interne au serveur';
                            break;
                    }

                    console.log('Edit error', error);
                    console.log('Hash was : ', hash);

                    let type: RequestType;

                    switch (editionType) {
                        case EditionType.ADDITION:
                            type = RequestType.ADDITION;
                            break;

                        case EditionType.FRAUD_REPORT:
                            type = RequestType.FRAUD_REPORT;
                            break;

                        case EditionType.INVALIDATION:
                            type = RequestType.INVALIDATION;
                            break;

                        case EditionType.REVALIDATION:
                            type = RequestType.REVALIDATION;
                            break;

                        default:
                            throw Error('Unsupported editionType');
                    }

                    const errorResult: EditionResult = {
                        errorCode,
                        errorMessage,
                        hash,
                        result,
                        type,
                        transactionID: null
                    };
                    editionRequestResponseObservable.next(errorResult);
                }
            );
        });

        return editionRequestResponseObservable;
    }

    private editDiplomaHashWithLogin(hash: string, login: string, editionType: EditionType): Observable<EditionResult> {
        const resourceUrl = this.getDiplomaEditionAPIUrl(editionType, hash, login);

        switch (editionType) {
            case EditionType.ADDITION:
                this.queriesStatusService.additionQueriesStatus.next({ hash, status: IQueryStatus.RUNNING });
                break;
            case EditionType.FRAUD_REPORT:
                this.queriesStatusService.fraudreportQueriesStatus.next({ hash, status: IQueryStatus.RUNNING });
                break;
            case EditionType.INVALIDATION:
                this.queriesStatusService.invalidationQueriesStatus.next({ hash, status: IQueryStatus.RUNNING });
                break;
            case EditionType.REVALIDATION:
                this.queriesStatusService.revalidationQueriesStatus.next({ hash, status: IQueryStatus.RUNNING });
                break;
            default:
                throw 'Unsuported editionType';
        }

        return this.httpClient.post(resourceUrl, {}).pipe(
            map(response => {
                const castedResponse: EditionResponse = <EditionResponse>response;

                let requestType: RequestType;
                switch (editionType) {
                    case EditionType.ADDITION:
                        requestType = RequestType.ADDITION;
                        break;
                    case EditionType.FRAUD_REPORT:
                        requestType = RequestType.FRAUD_REPORT;
                        break;
                    case EditionType.INVALIDATION:
                        requestType = RequestType.INVALIDATION;
                        break;
                    case EditionType.REVALIDATION:
                        requestType = RequestType.REVALIDATION;
                        break;
                    default:
                        throw 'Unsuported editionType';
                }

                const editionResult: EditionResult = {
                    type: requestType,
                    result: RequestResponseResult.SUCCESS,
                    hash,
                    transactionID: castedResponse.transactionID
                };

                return editionResult;
            })
        );
    }

    private getDiplomaEditionAPIUrl(editionType, hash, login): string {
        switch (editionType) {
            case EditionType.ADDITION:
                return SERVER_API_URL + `api/requests/add?login=${login}&hash=${hash}`;
            case EditionType.FRAUD_REPORT:
                return SERVER_API_URL + `api/requests/fraudulent?login=${login}&hash=${hash}`;
            case EditionType.INVALIDATION:
                return SERVER_API_URL + `api/requests/invalidate?login=${login}&hash=${hash}`;
            case EditionType.REVALIDATION:
                return SERVER_API_URL + `api/requests/revalidate?login=${login}&hash=${hash}`;
            default:
                throw 'Unsuported editionType';
        }
    }

    /* DIPLOMA CHECK SECTION */

    public checkFiles(files: IUploadedFile[]): Map<string, string> {
        const nameHashMap: Map<string, string> = new Map<string, string>();

        files.forEach((file: IUploadedFile) => {
            // ONLY FOR DEBUG
            // const computedHash: string = this.hashService.notWorkinghashIUploadedFile(file);

            const computedHash: string = this.hashService.hashIUploadedFile(file);
            const checkHashResult: ReplaySubject<CheckResult> = this.checkHash(computedHash);

            nameHashMap.set(computedHash, file.metadata.name);

            this.queriesStatusService.addCheckResult(checkHashResult, computedHash);
        });

        return nameHashMap;
    }

    private checkHash(hash: string): ReplaySubject<CheckResult> {
        // used ReplaySubject due to answer https://stackoverflow.com/a/33676723/6304206
        const checkRequestResponseObservable: ReplaySubject<CheckResult> = new ReplaySubject<CheckResult>(1);

        const resourceUrl = SERVER_API_URL + `api/requests/check?hash=${hash}`;

        this.queriesStatusService.checkQueriesStatus.next({ hash, status: IQueryStatus.RUNNING });

        const response: Observable<CheckResult> = this.httpClient.get<CheckResponse>(resourceUrl, {}).pipe(
            map(resp => {
                console.log('Received : ', resp);

                const castedResponse: CheckResponse = <CheckResponse>resp;
                const checkResponse: CheckResult = {
                    type: RequestType.CHECK,
                    result: RequestResponseResult.SUCCESS,
                    diplomaState: castedResponse.diplomaState,
                    hash
                };

                return checkResponse;
            })
        );

        response.subscribe(
            (data: CheckResult) => {
                checkRequestResponseObservable.next(data);
            },
            error => {
                const errorCode: number = error.status;
                let errorMessage: string;
                let result: RequestResponseResult;

                switch (error.status) {
                    case 406:
                        result = RequestResponseResult.CLIENT_ERROR;
                        errorMessage = "Le hash n'est pas présent dans la blockchain";
                        break;
                    case 417:
                        result = RequestResponseResult.CLIENT_ERROR;
                        switch (error.error) {
                            case 'INVALID HASH':
                                console.log('Detected "INVALID HASH" error code');
                                errorMessage = 'Le hash fourni est invalide';
                                break;
                            default:
                                console.log('Unsupported error of number ', error.status, ', using givent message : ', error.error);
                                errorMessage = error.error;
                                break;
                        }
                        break;
                    case 500:
                        result = RequestResponseResult.SERVER_ERROR;
                        errorMessage = 'Erreur interne au serveur';
                        break;
                }

                console.log('Check error : ', error);
                console.log('Hash was : ', hash);

                const errorResult: CheckResult = {
                    diplomaState: null,
                    errorCode,
                    errorMessage,
                    hash,
                    result,
                    type: RequestType.CHECK
                };
                checkRequestResponseObservable.next(errorResult);
            }
        );

        return checkRequestResponseObservable;
    }
}
