import { Injectable } from '@angular/core';
import { sha256 } from 'js-sha256';
import { IUploadedFile } from '../model/uploadedFile.model';

@Injectable({
    providedIn: 'root'
})
export class HashService {
    constructor() {}

    public hashArrayBuffer(arrayBuffer: ArrayBuffer): string {
        return sha256(arrayBuffer);
    }

    public hashIUploadedFile(uploadedFile: IUploadedFile): string {
        return this.hashArrayBuffer(uploadedFile.data);
    }

    // FOR DEBUG PURPOSE ONLY !
    public notWorkinghashIUploadedFile(uploadedFile: IUploadedFile): string {
        return 'do not use : wrong hash !';
    }
}
