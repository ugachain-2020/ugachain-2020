import { Injectable } from '@angular/core';

import { Subject } from 'rxjs';

import { IUploadedFile } from '../model/uploadedFile.model';

@Injectable({
    providedIn: 'root'
})
export class FileUploadService {
    private _uploadedFilesSubject = new Subject<IUploadedFile>();

    constructor() {}

    public buildUploadedFileList(files: FileList): IUploadedFile[] {
        const result: IUploadedFile[] = [];

        for (let i = 0; i < files.length; i++) {
            const reader = new FileReader();
            reader.onloadend = () => {
                // Cast possibility is ensured by call to 'readAsArrayBuffer'
                const currentUploadedFile: IUploadedFile = this.buildUploadedFile(files[i], <ArrayBuffer>reader.result);
                result.push(currentUploadedFile);
            };
            reader.readAsArrayBuffer(files[i]);
        }

        return result;
    }

    private buildUploadedFile(metadata: File, data: ArrayBuffer): IUploadedFile {
        return { metadata, data };
    }

    /* Getters & Setters */

    public get uploadedFilesSubject() {
        return this._uploadedFilesSubject;
    }
}
