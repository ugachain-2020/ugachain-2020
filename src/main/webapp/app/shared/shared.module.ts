import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';

import { NgbDateMomentAdapter } from './util/datepicker-adapter';
import { UgaChainSharedLibsModule, UgaChainSharedCommonModule, HasAnyAuthorityDirective } from './';

@NgModule({
    imports: [UgaChainSharedLibsModule, UgaChainSharedCommonModule],
    declarations: [HasAnyAuthorityDirective],
    providers: [{ provide: NgbDateAdapter, useClass: NgbDateMomentAdapter }],
    exports: [UgaChainSharedCommonModule, HasAnyAuthorityDirective],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UgaChainSharedModule {
    static forRoot() {
        return {
            ngModule: UgaChainSharedModule
        };
    }
}
