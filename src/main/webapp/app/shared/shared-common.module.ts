import { NgModule } from '@angular/core';

import { UgaChainSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [UgaChainSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [UgaChainSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class UgaChainSharedCommonModule {}
