import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { errorRoute, sidebarRoute } from './layouts';
import { DEBUG_INFO_ENABLED } from 'app/app.constants';

const LAYOUT_ROUTES = [sidebarRoute, ...errorRoute];

@NgModule({
    imports: [
        RouterModule.forRoot(
            [
                {
                    path: 'admin',
                    loadChildren: './admin/admin.module#UgaChainAdminModule'
                },
                ...LAYOUT_ROUTES
            ],
            { useHash: true, enableTracing: false }
        )
    ],
    exports: [RouterModule]
})
export class UgaChainAppRoutingModule {}
