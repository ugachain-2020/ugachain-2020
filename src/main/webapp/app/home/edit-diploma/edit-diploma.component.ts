import { Component, OnInit, Input } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Subject } from 'rxjs';

import * as lodash from 'lodash';

import { EditionType } from '../../shared/model/edition.model';
import { IUploadedFile } from '../../shared/model/uploadedFile.model';
import { IEditionQuery, IQueryStatus } from '../../shared/model/query.model';

import { ApiService } from '../../shared/services/api.service';
import { FileUploadService } from '../../shared/services/file-upload.service';
import { QueriesStatusService } from '../../shared/services/queries-status.service';

@Component({
    selector: 'jhi-edit-diploma',
    templateUrl: './edit-diploma.component.html',
    styleUrls: ['../../../content/css/upload-file.component.css']
})
export class EditDiplomaComponent implements OnInit {
    @Input() editionType: EditionType;
    editionTypePossibilities: typeof EditionType = EditionType;

    uploadedFiles: IUploadedFile[] = [];
    files: FileList;

    formGroup = this.fb.group({
        file: [null, Validators.required]
    });

    private editionQueriesSubject: Subject<IEditionQuery>;
    runningQueries: IEditionQuery[] = [];
    errorQueries: IEditionQuery[] = [];
    successQueries: IEditionQuery[] = [];

    constructor(
        private apiService: ApiService,
        private fb: FormBuilder,
        private fileUploadService: FileUploadService,
        private queriesStatusService: QueriesStatusService
    ) {}

    ngOnInit() {
        this.editionQueriesSubject = this.queriesStatusService.getQueriesStatusSubject(this.editionType);

        this.editionQueriesSubject.subscribe((query: IEditionQuery) => {
            switch (query.status) {
                case IQueryStatus.SUCCESS:
                    lodash.remove(this.runningQueries, (q: IEditionQuery) => q.hash === query.hash);
                    if (query.resultingTransactionID !== null) {
                        this.successQueries.push(query);
                    } else {
                        console.log('Error : Received successful query without resultingTransactionID');
                    }
                    break;

                case IQueryStatus.RUNNING:
                    console.log('Running query : ', query);
                    this.runningQueries.push(query);
                    break;

                case IQueryStatus.ERROR:
                    console.log('Error query : ', query);
                    lodash.remove(this.runningQueries, (q: IEditionQuery) => q.hash === query.hash);
                    this.errorQueries.push(query);
                    break;

                default:
                    console.log('Error : Unsupported IQueryStatus');
                    break;
            }
        });
    }

    //  public onFileChange(event): void {
    //  //In order to avoid resending old files
    //
    // }

    public uploadFile(event): void {
        this.emptyUploadedFilesList();

        if (event.target.files && event.target.files.length) {
            this.uploadedFiles = this.fileUploadService.buildUploadedFileList(event.target.files);
        }
    }

    public dragFile(event): void {
        console.log(event);
        this.emptyUploadedFilesList();

        if (event.length > 0) {
            this.uploadedFiles = this.fileUploadService.buildUploadedFileList(event);
        }
    }

    public deleteAttachement(index) {
        this.uploadedFiles.splice(index, 1);
    }

    public submit() {
        let filenameHashEditionMap: Map<string, string> = new Map<string, string>();
        console.log('enregistrement');

        filenameHashEditionMap = this.apiService.editDiplomas(this.uploadedFiles, this.editionType);

        // set new key-value pairs to filename-hash Map
        filenameHashEditionMap.forEach((value, key) => this.queriesStatusService.addFilenameHashPair(value, key));

        this.emptyUploadedFilesList();
        this.emptyQueriesResultsLists();
    }

    private emptyUploadedFilesList(): void {
        this.uploadedFiles = [];
    }

    private emptyQueriesResultsLists(): void {
        this.errorQueries = [];
        this.successQueries = [];
    }

    getQueryFilename(query: IEditionQuery): string {
        return this.queriesStatusService.getQueryFilename(query);
    }

    getComponentTitle(): string {
        switch (this.editionType) {
            case EditionType.ADDITION:
                return 'Ajout de diplôme';
            case EditionType.INVALIDATION:
                return 'Invalidation de diplôme';
            case EditionType.REVALIDATION:
                return 'Revalidation de diplôme';
            case EditionType.FRAUD_REPORT:
                return 'Déclaration de fraude de diplôme';
            default:
                throw Error('Erreur : type de composant non supporté');
        }
    }

    getSuccessMessage(query: IEditionQuery): string {
        switch (this.editionType) {
            case EditionType.ADDITION:
                return 'Le diplome ' + this.getQueryFilename(query) + ' a bien été ajouté à la blockchain';
            case EditionType.INVALIDATION:
                return 'Le diplome ' + this.getQueryFilename(query) + ' a bien été invalidé';
            case EditionType.REVALIDATION:
                return 'Le diplome ' + this.getQueryFilename(query) + ' a bien été revalidé';
            case EditionType.FRAUD_REPORT:
                return 'Le diplome ' + this.getQueryFilename(query) + ' a bien été déclaré frauduleux';
            default:
                throw Error('Erreur : type de composant non supporté');
        }
    }

    getCardBodyText(): string {
        return '\n \n Gisser vos fichiers ici ou cliquer';
    }

    getSubmitButtonName(): string {
        switch (this.editionType) {
            case EditionType.ADDITION:
                return 'Ajouter';
            case EditionType.INVALIDATION:
                return 'Invalider';
            case EditionType.REVALIDATION:
                return 'Revalider';
            case EditionType.FRAUD_REPORT:
                return 'Déclarer frauduleux';
            default:
                return 'Envoyer';
        }
    }

    getUploadButtonName(): string {
        switch (this.editionType) {
            case EditionType.ADDITION:
                return ' Téléverser les fichiers à ajouter';
            case EditionType.FRAUD_REPORT:
                return ' Téléverser les fichiers à déclarer frauduleux';
            case EditionType.INVALIDATION:
                return ' Téléverser les fichiers à invalider';
            case EditionType.REVALIDATION:
                return ' Téléverser les fichiers à revalider';
            default:
                throw Error('Unsupported edition type');
        }
    }
}
