import { Component, OnInit } from '@angular/core';
import { NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';
import { LoginService, AccountService, Account } from 'app/core';
import { EditionType } from 'app/shared/model/edition.model';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: ['home.css'],
    providers: [NgbAlertConfig]
})
export class HomeComponent implements OnInit {
    account: Account;

    additionEditionType: EditionType = EditionType.ADDITION;
    invalidationEditionType: EditionType = EditionType.INVALIDATION;
    revalidationEditionType: EditionType = EditionType.REVALIDATION;
    fraudReportEditionType: EditionType = EditionType.FRAUD_REPORT;

    staticAlertClosed = false;

    constructor(private accountService: AccountService, private loginService: LoginService, alertConfig: NgbAlertConfig) {
        alertConfig.type = 'warning';
    }

    ngOnInit() {
        this.accountService.identity().then((account: Account) => {
            this.account = account;
        });
    }

    isAuthenticated() {
        return this.accountService.isAuthenticated();
    }

    login() {
        this.loginService.login();
    }
}
