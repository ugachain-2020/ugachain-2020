import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';

import * as lodash from 'lodash';

import { ApiService } from 'app/shared/services/api.service';
import { FileUploadService } from 'app/shared/services/file-upload.service';
import { QueriesStatusService } from 'app/shared/services/queries-status.service';

import { DiplomaState } from 'app/shared/model/diplomaState.model';
import { ICheckQuery, IQueryStatus } from 'app/shared/model/query.model';
import { IUploadedFile } from 'app/shared/model/uploadedFile.model';

@Component({
    selector: 'jhi-check-diploma',
    templateUrl: './check-diploma.component.html',
    styleUrls: ['../../../content/css/upload-file.component.css']
})
export class CheckDiplomaComponent implements OnInit {
    uploadedFiles: IUploadedFile[] = [];

    files: FileList;

    formGroup = this.fb.group({
        file: [null, Validators.required]
    });

    ongoingQueries: ICheckQuery[] = [];
    errorQueries: ICheckQuery[] = [];
    validQueries: ICheckQuery[] = [];
    invalidQueries: ICheckQuery[] = [];
    fraudulentQueries: ICheckQuery[] = [];
    notFoundQueries: ICheckQuery[] = [];

    private queriesSubject: Subject<ICheckQuery>;

    constructor(
        private apiService: ApiService,
        private fb: FormBuilder,
        private fileUploadService: FileUploadService,
        private queriesStatusService: QueriesStatusService
    ) {}

    ngOnInit() {
        this.queriesSubject = this.queriesStatusService.checkQueriesStatus;
        this.queriesSubject.subscribe((query: ICheckQuery) => {
            switch (query.status) {
                case IQueryStatus.SUCCESS:
                    lodash.remove(this.ongoingQueries, (q: ICheckQuery) => q.hash === query.hash);
                    if (query.resultingDiplomaState !== null) {
                        this.dispatchSucessfulQuery(query);
                    } else {
                        console.log('Error : Received successful query without resultingDiplomaState');
                    }
                    break;

                case IQueryStatus.RUNNING:
                    console.log('Running query : ', query);
                    this.ongoingQueries.push(query);
                    break;

                case IQueryStatus.ERROR:
                    console.log('Error query : ', query);
                    console.log(query.errorCode, query.errorMessage);
                    lodash.remove(this.ongoingQueries, (q: ICheckQuery) => q.hash === query.hash);
                    this.errorQueries.push(query);
                    break;

                default:
                    console.log('Error : Unsupported IQueryStatus');
                    break;
            }
        });
    }

    // public onFileChange(event): void {
    //     // In order to avoid resending old files
    //     this.emptyUploadedFilesList();

    //     if (event.target.files && event.target.files.length) {
    //         this.uploadedFiles = this.fileUploadService.buildUploadedFileList(event.target.files);
    //     }
    // }

    public uploadFile(event): void {
        console.log('bite');
        this.emptyUploadedFilesList();
        if (event.target.files && event.target.files.length) {
            this.uploadedFiles = this.fileUploadService.buildUploadedFileList(event.target.files);
        }
    }

    public dragFile(event): void {
        // for(let i = 0; i < event.length; i++){
        //         this.files.item(event[i]);
        //     }

        this.emptyUploadedFilesList();
        if (event.length > 0) {
            this.uploadedFiles = this.fileUploadService.buildUploadedFileList(event);
        }
    }

    public deleteAttachement(index) {
        this.uploadedFiles.splice(index, 1);
    }

    public submit() {
        const fileCheckMap: Map<string, string> = this.apiService.checkFiles(this.uploadedFiles);
        // set new key-value pairs to nameHashMap
        fileCheckMap.forEach((value, key) => this.queriesStatusService.addFilenameHashPair(value, key));

        this.emptyQueriesResultsLists();
        this.emptyUploadedFilesList();
    }

    private emptyUploadedFilesList(): void {
        this.uploadedFiles = [];
    }

    private emptyQueriesResultsLists(): void {
        this.errorQueries = [];
        this.validQueries = [];
        this.invalidQueries = [];
        this.fraudulentQueries = [];
        this.notFoundQueries = [];
    }

    private dispatchSucessfulQuery(checkQuery: ICheckQuery): void {
        switch (checkQuery.resultingDiplomaState) {
            case DiplomaState.NOT_FOUND:
                console.log('Not_Found diploma : ', checkQuery);
                this.notFoundQueries.push(checkQuery);
                break;

            case DiplomaState.VALID:
                console.log('Valid diploma : ', checkQuery);
                this.validQueries.push(checkQuery);
                break;

            case DiplomaState.INVALID:
                console.log('Invalid diploma : ', checkQuery);
                this.invalidQueries.push(checkQuery);
                break;

            case DiplomaState.FRAUDULENT:
                console.log('Fraudulent diploma : ', checkQuery);
                this.fraudulentQueries.push(checkQuery);
                break;

            default:
                console.log('Error : Unsupported DiplomaState');
                break;
        }
    }

    getQueryFilename(query: ICheckQuery): string {
        return this.queriesStatusService.getQueryFilename(query);
    }
}
