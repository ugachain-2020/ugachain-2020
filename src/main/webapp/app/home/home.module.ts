import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { QRCodeModule } from 'angularx-qrcode';

import { CheckDiplomaComponent } from './check-diploma/check-diploma.component';
import { EditDiplomaComponent } from './edit-diploma/edit-diploma.component';
import { HOME_ROUTE, HomeComponent } from './';
import { UgaChainSharedModule } from 'app/shared';
import { DragDropDirective } from 'app/home/drag-drop.directive';

@NgModule({
    imports: [UgaChainSharedModule, RouterModule.forChild([HOME_ROUTE]), ReactiveFormsModule, QRCodeModule],
    declarations: [HomeComponent, CheckDiplomaComponent, EditDiplomaComponent, DragDropDirective],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UgaChainHomeModule {}
