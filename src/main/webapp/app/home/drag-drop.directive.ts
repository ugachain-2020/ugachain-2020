import { Directive, Output, Input, EventEmitter, HostBinding, HostListener } from '@angular/core';

@Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[jhi-dragdrop]'
})
export class DragDropDirective {
    @Output() onFileDropped = new EventEmitter<Array<File>>();

    @HostBinding('style.background-color') public background = '#f5fcff';
    @HostBinding('style.opacity') public opacity = '1';

    // Dragover listener
    @HostListener('dragover', ['$event']) onDragOver(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        this.background = '#9ecbec';
        this.opacity = '0.8';
    }

    // Dragleave listener
    @HostListener('dragleave', ['$event']) public onDragLeave(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        this.background = '#f5fcff';
        this.opacity = '1';
    }

    // Drop listener
    @HostListener('drop', ['$event']) public ondrop(evt) {
        // evt.preventDefault();
        // evt.stopPropagation();
        // this.background = '#f5fcff';
        // this.opacity = '1';
        // const files = evt.dataTransfer.files;
        // if (files.length > 0) {
        //   this.onFileDropped.emit(files);
        // }

        evt.preventDefault();
        evt.stopPropagation();
        if (evt.dataTransfer.items) {
            const files = [];
            for (let i = 0; i < evt.dataTransfer.items.length; i++) {
                // If dropped items aren't files, reject them
                if (evt.dataTransfer.items[i].kind === 'file') {
                    files.push(evt.dataTransfer.items[i].getAsFile());
                }
            }
            evt.dataTransfer.items.clear();
            this.onFileDropped.emit(files);
        } else {
            const files = evt.dataTransfer.files;
            evt.dataTransfer.clearData();
            this.onFileDropped.emit(Array.from(files));
        }
    }
}
