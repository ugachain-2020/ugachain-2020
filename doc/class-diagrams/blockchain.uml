@startuml

namespace blockchain {

	namespace request{
	}
	
	namespace blockchainException{
	}

	class CAClient [[java:fr.uga.ugachain.blockchain.CAClient]] {
		~String caUrl
		~Properties caProperties
		~HFCAClient instance
		~UserContext adminContext
		+UserContext getAdminUserContext()
		+void setAdminUserContext(UserContext userContext)
		+CAClient(String caUrl, Properties caProperties)
		+void init()
		+HFCAClient getInstance()
		+UserContext enrollAdminUser(String username, String password)
		+String registerUser(String username, String organization)
		+UserContext enrollUser(UserContext user, String secret)
	}
	
	class CAEnrollment [[java:fr.uga.ugachain.blockchain.CAEnrollment]] {
		-{static}long serialVersionUID
		-PrivateKey key
		-String cert
		+CAEnrollment(PrivateKey pkey, String signedPem)
		+PrivateKey getKey()
		+String getCert()
	}
	
	class ChannelClient [[java:fr.uga.ugachain.blockchain.ChannelClient]] {
		~String name
		~Channel channel
		~FabricClient fabClient
		+String getName()
		+Channel getChannel()
		+FabricClient getFabClient()
		+ChannelClient(String name, Channel channel, FabricClient fabClient)
		+Collection<ProposalResponse> queryByChainCode(String chaincodeName, String functionName, String[] args)
		+Collection<ProposalResponse> sendTransactionProposal(TransactionProposalRequest request)
		+Collection<ProposalResponse> instantiateChainCode(String chaincodeName, String version, String chaincodePath, String language, String functionName, String[] functionArgs, String policyPath)
		+TransactionInfo queryByTransactionId(String txnId)
	}
	
	class Config [[java:fr.uga.ugachain.blockchain.Config]] {
		+{static}String ORG1_MSP
		+{static}String ORG1
		+{static}String ORG2_MSP
		+{static}String ORG2
		+{static}String ADMIN
		+{static}String ADMIN_PASSWORD
		+{static}String CHANNEL_CONFIG_PATH
		+{static}String ORG1_USR_BASE_PATH
		+{static}String ORG2_USR_BASE_PATH
		+{static}String ORG1_USR_ADMIN_PK
		+{static}String ORG1_USR_ADMIN_CERT
		+{static}String ORG2_USR_ADMIN_PK
		+{static}String ORG2_USR_ADMIN_CERT
		+{static}String CA_ORG1_URL
		+{static}String CA_ORG2_URL
		+{static}String ORDERER_URL
		+{static}String ORDERER_NAME
		+{static}String CHANNEL_NAME
		+{static}String ORG1_PEER_0
		+{static}String ORG1_PEER_0_URL
		+{static}String ORG1_PEER_1
		+{static}String ORG1_PEER_1_URL
		+{static}String ORG2_PEER_0
		+{static}String ORG2_PEER_0_URL
		+{static}String ORG2_PEER_1
		+{static}String ORG2_PEER_1_URL
		+{static}String CHAINCODE_ROOT_DIR
		+{static}String CHAINCODE_1_NAME
		+{static}String CHAINCODE_1_PATH
		+{static}String CHAINCODE_1_VERSION
	}
	
	class FabricClient [[java:fr.uga.ugachain.blockchain.FabricClient]] {
		-HFClient instance
		+HFClient getInstance()
		+FabricClient(User context)
		+ChannelClient createChannelClient(String name)
		+Collection<ProposalResponse> deployChainCode(String chainCodeName, String chaincodePath, String codepath, String language, String version, Collection<Peer> peers)
	}
	
	class UserContext [[java:fr.uga.ugachain.blockchain.UserContext]] {
		-{static}long serialVersionUID
		#String name
		#Set<String> roles
		#String account
		#String affiliation
		#Enrollment enrollment
		#String mspId
		+void setName(String name)
		+void setRoles(Set<String> roles)
		+void setAccount(String account)
		+void setAffiliation(String affiliation)
		+void setEnrollment(Enrollment enrollment)
		+void setMspId(String mspId)
		+String getName()
		+Set<String> getRoles()
		+String getAccount()
		+String getAffiliation()
		+Enrollment getEnrollment()
		+String getMspId()
	}
	
	class Util [[java:fr.uga.ugachain.blockchain.Util]] {
		+{static}void writeUserContext(UserContext userContext)
		+{static}UserContext readUserContext(String affiliation, String username)
		+{static}CAEnrollment getEnrollment(String keyFolderPath, String keyFileName, String certFolderPath, String certFileName)
		+{static}void cleanUp()
		+{static}boolean deleteDirectory(File dir)
	}
	
}

@enduml